<?php

/**
 * @file
 * Administration page callbacks for the New Book by Default module.
 */

/**
 * Form builder. Set content types.
 *
 * TODO: /admin/content/book/settings is probably a better place.
 */
function new_book_by_default_admin_settings() {
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $options[$node_type->type] = $node_type->name;
  }

  $form['new_book_by_default_node_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Content types to be new books by default'),
    '#options'       => $options,
    '#default_value' => variable_get('new_book_by_default_node_types', array('book')),
    '#description'   => t('All new nodes of these content types will create, by default, a new book.'),
  );

  return system_settings_form($form);
}
