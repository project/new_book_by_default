New book by default is a lightweight module that allows administrators
to set which content types (e.g. Book page) will have the option
"<create a new book>" enabled by default.

This is useful, for example, when you want authors to create and
structure content using the Book outline hierarchy.

Dependencies
------------
 - Book core module

Installation
------------
 - Download and unpack the module to Drupal custom module's directory.
 - Enable the module. You will also have to enable the Book module if
   you haven't done it already.

Author
------
Pere Orga <pere@orga.cat>
